import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { runtimeEnvironment } from '../../environments/runtimeEnvironment';
import {map} from 'rxjs/operators';
import {Duty} from '../model/duty';

@Injectable({
  providedIn: 'root'
})
export class ToDoListService {

  private API_URL = runtimeEnvironment.apiUrl + 'list';

  constructor(private http: HttpClient) { }

  /**
   * Api call to retrieve list of activities
   */
  public getListOfActivities() {
    return this.http.get<Duty[]>(this.API_URL)
      .pipe(map(data => {
        return data;
      }));
  }

  /**
   * Api call to add a task to list
   * @string name
   */
  public addTaskToList(name: string) {
    const param = {
      name: name
    };

    return this.http.post<Duty>(this.API_URL, param, {observe: 'response'})
      .pipe(map(data => {
        return data;
      }));
  }

  /**
   * Api call to delete a selected task from the list
   * @number id
   */
  public deleteTaskFromList(id: number) {
    return this.http.delete<Duty>(this.API_URL + '/' + id, {observe: 'response'})
      .pipe(data => {
        return data;
      });
  }

  /**
   * Api call to update a selected task from the list
   * @number idTask
   * @string taskName
   */
  public updateTaskFromList(idTask: number, taskName: string) {
    const param = {
      name: taskName
    };

    return this.http.patch<Duty>(this.API_URL + '/' + idTask, param, {observe: 'response'})
      .pipe(data => {
        return data;
      });
  }
}
