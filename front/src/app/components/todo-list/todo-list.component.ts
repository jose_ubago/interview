import { Component, OnInit, OnDestroy } from '@angular/core';
import {ToDoListService} from '../../services/to-do-list.service';
import {FormBuilder, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Duty} from '../../model/duty';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit, OnDestroy {

  public newTaks: string;
  public listForm: FormGroup;
  public taskList: Duty[] = [];
  public editTask: boolean;
  private taskToUpdateId: number;

  private taskList$: Subscription;
  private addTaskList$: Subscription;
  private deleteTaskList$: Subscription;
  private updateTaskList$: Subscription;

  constructor(private todoListService: ToDoListService, private formBuilder: FormBuilder,) { }

  ngOnInit() {
    this.getList();

    this.listForm = this.formBuilder.group({
      listTask: ['', Validators.required]
    });
  }

  ngOnDestroy(): void {
    this.taskList$.unsubscribe();
    this.addTaskList$.unsubscribe();
    this.deleteTaskList$.unsubscribe();
    this.updateTaskList$.unsubscribe();
  }

  /**
   * Convenience getter for easy access to form fields
   */
  get formValues() {
    return this.listForm.controls;
  }

  /**
   * Api call to get list of activities
   */
  public getList(): void {
    this.taskList$ = this.todoListService.getListOfActivities().subscribe(result => this.taskList = result);
  }

  /**
   * Submits form data and either adds it to the task list or updates a selected task
   */
  public onSubmit(formDirective: FormGroupDirective): void {
    if (this.listForm.invalid) {
      return;
    }

    if (!this.editTask) {
      this.addTaskList$ = this.todoListService.addTaskToList(this.formValues.listTask.value).subscribe(result => {
        if (result.status === 201) {
          this.getList();
        }
      });
    } else {
      this.updateTaskList$ = this.todoListService.updateTaskFromList(this.taskToUpdateId, this.formValues.listTask.value)
        .subscribe(result => {
        if (result.status === 200) {
          this.getList();
          this.editTask = false;
        }
      });
    }
    formDirective.resetForm();
    this.listForm.reset();
  }

  /**
   * Deletes task from task list by id
   * @number id
   */
  public deleteTaks(id: number): void {
    this.deleteTaskList$ = this.todoListService.deleteTaskFromList(id).subscribe(result =>{
      if (result.status === 200) {
        this.getList();
      }
    });
  }

  /**
   * Sets variables for updating a selected task
   * @Duty task
   */
  public updateTask(task: Duty): void {
    this.editTask = true;
    this.newTaks = task.name;
    this.taskToUpdateId = task.id;
  }
}
