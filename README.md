# Interview exercise
**Company:** Dimatica

**Exercise:** TODO list

**Stack:** Angular 9 & NodeJS

**Proposed role:** Front-end developer

**Name:** Jose Martínez de Ubago


---


## Install projects

**Back**

Download `json-server-master` folder.

Access a terminal in the project root and execute `npm install -g json-server` to install project dependencies.

When project dependencies are installed, run `json-server --watch db.json` for a dev server. Port `3000` will be used by default.

**Front**

Download `front` folder.

Access a terminal in the project root and execute `npm install` to install project dependencies.

When project dependencies are installed, run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 
